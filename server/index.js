// importando express 
const express = require('express');
const router = require('./routers')
const path = require('path');
const bodyParser = require('body-parser');
// verficar si estamos en desarrollo o produccion
const configs = require('./config');
require('dotenv').config({path: 'variables.env'});
// basedatos
// const db = require('./config/database');
// probando conexion
// db.authenticate()
//     .then( ()=> console.log('DB conectada'))
//     .catch( err => console.log(err))

// configurando express
const app = express();

// habilitar pug
app.set('view engine', 'pug');
// añadir las vistas
app.set('views', path.join(__dirname, './views'));

// cargar una carpeta estatica public
app.use(express.static('public'));

// verficar si estamos en desarrollo o produccion
const config = configs[app.get('env')];
// creamo la variable del nombre del sitio
app.locals.titulo = config.nombresitio;

// muestra el año actual y genera la ruta
app.use( (req, res, next)=> {
    // crear nueva fecha
    const fecha = new Date();
    // variables locales
    res.locals.fechaActual = fecha.getFullYear();
    // ruta active
    res.locals.ruta = req.path;

    return next();
}) 

// ejecutando body parser
app.use(bodyParser.urlencoded({extended: true}));

// cargar las rutas
app.use('/', router());

// puerto y host
const host = process.env.HOST || '0.0.0.0';
const port = process.env.PORT || 3000
app.listen(port, host, () => {
    console.log('el servidor esta funcionando');
});

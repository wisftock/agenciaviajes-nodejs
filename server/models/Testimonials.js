const sequelize = require('sequelize');
const db = require('../config/database');
const Testimonials = db.define('testimonials',{
    name: {
        type: sequelize.STRING
    },
    email: {
        type: sequelize.STRING
    },
    messaje: {
        type: sequelize.STRING
    }
});
module.exports = Testimonials;
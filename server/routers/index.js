const express = require('express');
const router = express.Router();

// controllers
const travelController = require('../controllers/travelController');
const testimonialController = require('../controllers/testimonialController');
const homeController = require('../controllers/homeController');

module.exports = function(){
    router.get('/', homeController.homeController);
    router.get('/travel', travelController.Allviajes);
    router.get('/travel/:id', travelController.Allviaje );
    router.get('/testimonial', testimonialController.AllTestimonials);
    // cuando se llena el formulario
    router.post('/testimonial', testimonialController.AllTestimonial);

    return router;
}
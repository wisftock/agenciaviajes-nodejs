const sequelize = require('sequelize');
require('dotenv').config({path: 'variables.env'});
module.exports = new sequelize(process.env.DB_NOMBRE, 
    process.env.DB_USER, 
    process.env.DB_PASS, {
    hots: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: 'mysql',
    define: {
        timestamps: false
    },
    pool: {
        max: 5,
        min: 0,
        acquire: 3000,
        idle: 10000
    }
});
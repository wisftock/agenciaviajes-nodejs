const Testimonial = require('../models/Testimonials');

exports.AllTestimonials = async(req, res) => {
    const testimonial = await Testimonial.findAll()
        res.render('testimonial', {
            pagina: 'Testimonials', testimonial
        });
}

exports.AllTestimonial =  (req, res) =>{
    // validando los campos completos
    let {name, email, messaje} = req.body;
    let errores = [];
    if(!name){
        errores.push({'mensaje': 'Enter name'})
    }
    if(!email){
        errores.push({'mensaje': 'Enter email'})
    }
    if(!messaje){
        errores.push({'mensaje': 'Enter messaje'})
    }
    // revisar por errores 
    if(errores.length > 0){
        // muestra la vista con errores
        res.render('/testimonial', {
            errores,
            name,
            email,
            messaje
        });
    } else {
        // almacenar en la base de datos
        Testimonial.create({
            name,
            email,
            messaje
        })
        .then(testimonial => res.redirect('/testimonial'))
        .catch(error => console.log(error))
    }
}

/* exports.AllTestimonials = (req, res) => {
    Testimonial.findAll()
        .then(testimonial => res.render('testimonial', {
            pagina: 'Testimonials', testimonial
        }))
        .catch(err => console.log(err))
}

exports.AllTestimonial =  (req, res) =>{
    // validando los campos completos
    let {name, email, messaje} = req.body;
    let errores = [];
    if(!name){
        errores.push({'mensaje': 'Enter name'})
    }
    if(!email){
        errores.push({'mensaje': 'Enter email'})
    }
    if(!messaje){
        errores.push({'mensaje': 'Enter messaje'})
    }
    // revisar por errores 
    if(errores.length > 0){
        // muestra la vista con errores
        res.render('/testimonial', {
            errores,
            name,
            email,
            messaje
        });
    } else {
        // almacenar en la base de datos
        Testimonial.create({
            name,
            email,
            messaje
        })
        .then(testimonial => res.redirect('/testimonial'))
        .catch(error => console.log(error))
    }
} */
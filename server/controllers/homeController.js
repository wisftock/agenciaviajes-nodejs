const Testimonial = require('../models/Testimonials');
const Viaje = require('../models/Viajes');

exports.homeController = async (req, res) => {
    const viajes = await Viaje.findAll({ limit: 3}) ;
    const testimonial = await Testimonial.findAll( {limit: 6});

    res.render('index', {
        viajes ,
        testimonial 
    });
}

/* exports.homeController = (req, res) => {
    const promise = [];
    promise.push( Viaje.findAll({ limit: 3}) );
    promise.push( Testimonial.findAll( {limit: 6}));
    // pasar el promise y ejecutarlo
    const resultado = Promise.all(promise);

    resultado.then(resultado => res.render('index', {
            pagina: 'Next Trips', 
            viajes : resultado[0],
            testimonial : resultado[1]
        }))
        .catch(err => console.log(err))
} */